package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
            initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                }
                else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int x = 0; x < currentGeneration.numRows(); x++) {
            for (int y = 0; y < currentGeneration.numColumns(); y++) {
                nextGeneration.set(x,y, getNextCell(x,y));
            }
        }
        currentGeneration = nextGeneration;

    }

    @Override
    public CellState getNextCell(int row, int col) {
        if (getCellState(row, col).equals(CellState.ALIVE)) {
            return CellState.DYING;
        }
        if (getCellState(row, col).equals(CellState.DEAD)) {
            if (countNeighbors(row, col)==2) {
                return CellState.ALIVE;
            }
        }
        return CellState.DEAD;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    private int countNeighbors(int row, int col) {
        int counter = 0;
        for (int x =-1; x < 2; x++) {
            for (int y = -1; y < 2; y++) {
                if (getCellState(row + x, col + y).equals(CellState.ALIVE)) {
                    counter++;
                }
            }
        }
        return counter;
    }
}
